module "eks-cluster" {
  source  = "terraform-aws-modules/eks/aws"
  version = "19.20.0"

  cluster_name = "app-eks-cluster"
  cluster_version = "1.28"

  subnet_ids = module.app-vpc.private_subnets
  vpc_id = module.app-vpc.vpc_id

  tags = {
    Environment = "dev"
    Application = "app"
  }

  eks_managed_node_groups = {
    dev = {
      min_size     = 1
      max_size     = 3
      desired_size = 3

      instance_types = ["t2.small"]
    
    }
  }
}