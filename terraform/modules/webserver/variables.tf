variable image_name{}
variable env_prefix{}
variable vpc_id{}
variable instance_type{}
variable public_key_location{}
variable user_data_location{}
variable avail_zone{}
variable subnet_id{}
