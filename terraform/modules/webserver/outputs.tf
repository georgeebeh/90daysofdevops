output "aws_ami_id" {
    value = data.aws_ami.my_ubuntu_nginx.id
}
output "ec2_instance" {
    value = aws_instance.app_server
}