terraform{
    required_providers {
      aws = {
        source = "hashicorp/aws"
        version = ">=4.3.0"
        }   
    }
}

provider "aws" {
    region = "eu-west-2"
}


resource "aws_vpc" "app_vpc" {
    cidr_block = var.vpc_cidr_block
    tags = {
        Name: "${var.env_prefix}-vpc"
    }
}

module "app-subnet" {
    source = "./modules/subnet"
    subnet_cidr_block = var.subnet_cidr_block
    avail_zone = var.avail_zone
    env_prefix = var.env_prefix
    vpc_id = aws_vpc.app_vpc.id

}

module "app-server" {
    source = "./modules/webserver"
    image_name = var.image_name
    env_prefix = var.env_prefix
    vpc_id = aws_vpc.app_vpc.id
    instance_type = var.instance_type
    public_key_location = var.public_key_location
    user_data_location = var.user_data_location
    avail_zone = var.avail_zone
    subnet_id = module.app-subnet.subnet.id
}